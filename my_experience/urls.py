from django.urls import re_path, path
from .views import my_experience

urlpatterns = [
    # path('my_experience', my_experience),
    re_path(r'^my_experience/', my_experience),
]