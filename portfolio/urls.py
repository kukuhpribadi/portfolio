from django.urls import re_path, path
from .views import portfolio

urlpatterns = [
    re_path(r'^portfolio/', portfolio),
]
