from django.urls import re_path, path
from .views import resume

urlpatterns = [
    re_path(r'^resume/', resume),
]
